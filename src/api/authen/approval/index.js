import fetch from 'utils/fetch';

export function getApprovalList(query) {
  return fetch({
    url: '/api/admin/approval/page',
    method: 'get',
    params:query
  });
}

export function getApprovalDetail(id){
	return fetch({
	    url: '/api/admin/approval/detail/'+id,
	    method: 'get'
	  });
}

export function saveApproval(approval){
	return fetch({
	    url: '/api/admin/approval/save',
	    method: 'post',
	    data:apply
	  });
}

export function updateApproval(approval,id){
	return fetch({
	    url: '/api/admin/approval/'+id,
	    method: 'put',
	    data:apply
	  });
}

export function deletApproval(id){
	return fetch({
	    url: '/api/admin/approval/'+id,
	    method: 'delete'
	  });
}
