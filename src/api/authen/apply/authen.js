import fetch from 'utils/fetch';

export function getAuthenList(query){
	return fetch({
		url:'/api/admin/record/page',
		method:'get',
		params:query
	});
}

export function getApprovalDetail(id){
	return fetch({
		url:'/api/admin/record/'+id,
		method:'get'
	});
}
export function getLastRecord(id){
	return fetch({
		url:'/api/admin/record/authen/'+id,
		method:'get'
	});
}

export function doAuthen(record){
	return fetch({
		url:'/api/admin/record/audit',
		method:'post',
		data:record
	});
}