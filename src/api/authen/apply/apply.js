import fetch from 'utils/fetch';

export function getListByUser(query) {
  return fetch({
    url: '/api/admin/apply/list',
    method: 'get',
    params:query
  });
}
export function getApplyList(query) {
  return fetch({
    url: '/apply/page',
    method: 'get',
    params:query
  });
}

export function getApplyDetail(id){
	return fetch({
	    url: '/api/admin/apply/detail/'+id,
	    method: 'get'
	  });
}


export function getApplyPrint(id){
	return fetch({
	    url: '/api/admin/apply/print/'+id,
	    method: 'get'
	  });
}

export function saveApply(apply){
	return fetch({
	    url: '/api/admin/apply/save',
	    method: 'post',
	    data:apply
	  });
}

export function updateApply(id,apply){
	return fetch({
	    url: '/api/admin/apply/'+id,
	    method: 'put',
	    data:apply
	  });
}

export function deletApply(id){
	return fetch({
	    url: '/api/admin/apply/'+id,
	    method: 'delete'
	  });
}
